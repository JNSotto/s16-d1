//Loops

/*Instruction: Display " Juan Dela Cruz" on our console 10x*/
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");

/*Instruction: Display each element available on our array*/

let studentsBootcamp = ['TJ', "Mia", 'Tin', "Chist"];

console.log(studentsBootcamp[0]);
console.log(studentsBootcamp[1]);
console.log(studentsBootcamp[2]);
console.log(studentsBootcamp[3]);





// While Loop

	let count =5; //number of the iteration, number of times of how many we repeat our code 

	// while (/*condition*/){ // condition - evaluates a given code if it is true or false - if the condition is true, the loop will start and continue our iteration or the repetition of our block of code, but if the condition if false the loop or the repetition will stop.

	// //block of code - this will be repeated by the loop 
	// //counter for our iteration - this is the reason of continuous loop/iteration 
	// }
	
	/*
		example 
			Instructions: Repeat a name "Sylvan" 5x. 
	 */

while(count !== 0){ //condition - if count value if not equal to zero 
	console.log("Sylvan");
	count--; //will be decremented by 1
	
}

// Instruction: Print numbers 1 to 5 using while

let number = 1; 

while(number <= 5){ //if the number is less than or equal to 5
	console.log(number);
	number++; 
}

// Mini - Activity 
// Instruction: With a given array, kindly print each element using while loop
let fruits = ['Banana', 'Mango'];
//fruits[0]
//fruits[1]

let indexNumber = 0;//We will use this variable as our reference to the index position/number of our given array

while (indexNumber <= 1){// the condition is based on the last index of elements that we have on array 
	console.log(fruits[indexNumber]);// kukuhanin natin yung elements sa loob ng array base sa indexNumber value
	indexNumber++; 
}

let mobilePhones = ['Samsung Galaxy s21', 'Iphone 13 pro', 'Xioami 11T', 'Realme C', 'Huawei Nova 8', 'Pixel 5', 'Asus Rog 6','Nokia','Cherry Mobile']; 

console.log(mobilePhones.length);// putting .length in a array, it can count the number of element in the array
console.log(mobilePhones.length - 1);//mobilePhones.length - 1, this will give us the last index position of an element in an array accurately
console.log(mobilePhones[mobilePhones.length - 1]);//[mobilePhones.length - 1], this get the last element of an array accurately. 

let indexNumberForMobile = 0; 

while(indexNumberForMobile <= mobilePhones.length-1){
	console.log(mobilePhones[indexNumberForMobile])
	indexNumberForMobile++;
}




//Do-While Loop - do the statement once, before going to the condition

let countA =1; 

do { //execute the statement first
	console.log('Juan');
	countA++; 
} while(countA <= 6);


console.log("++++++++++++++++ Do-While VS While +++++++++++++++");// separator lang 

let countB = 6; 

do {
	console.log(`Do-While count ${countB}`);
	countB--; 
} while(countB == 7); 

/*Versus*/

while(count == 7){
	console.log(`Do-While count ${countB}`);
	countB--;
}; 

//Mini-Activity 
//Instruction: With a given array, kindly display each elements on the console using do-while loop

let indexNumberA =0; 

let computerBrands = ['Apple Macbook Pro', 'Hp NoteBook', 'Asus', 'Lenovo', 'Acer', 'Dell', 'Huawei']; 

do {
	console.log(computerBrands[indexNumberA]);
	indexNumberA++;
}while(indexNumberA <= computerBrands.length-1);





//For Loop - it is more flexible than while and do-while loops. 
	//variable - the scope of the declared variable is within the For loop
	//condition 
	//iteration 
for(let count= 5; count >= 0; count--){
	console.log(count);
}

//Mini-Activity 
// Instruction: Given an Array, kindly print each element using For loop

let colors = ['Red', 'Green', 'Blue', 'Purple', 'White', 'Black'];

for(let i=0; i <= colors.length-1; i++){
	console.log(colors[i]);
}

//Continue & Break
//Break - stops the execution of our code
//Continue - skip a block code and continue to the next iteration

/*
Ages:
	18, 19, 20, 21, 24, 25

	age == 21 (debutante age of boys), we will skip then to the next iteration

	18, 19, 24, 25
 */

let ages = [18, 19, 20, 21, 24, 25];/*Skip the debutante of boys and girls using Continue Keywork*/

for (let i=0; i <= ages.length -1; i++){
		//age[0]=18 - skip OR //age[3]=21 - skip
	if(ages[i] == 21 || ages[i] == 18){
		continue;
	}
	console.log(ages[i]);
}

/*
	let studentName= ['Den', 'Jayson', 'Marvin', 'Rommel']; 

	Once we found Jayson on our array, we will stop the loop 
 */

let studentsName= ['Den', 'Jayson', 'Marvin', 'Rommel']; 

for (let i=0; i <= studentsName.length-1; i++){

	if(studentsName[i] == 'Jayson'){
		console.log(studentsName[i]);
		break;
	}
}

//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

/*
	Sample output:
  
  20
  23
  33
  27
  70
  55
  63
  85
*/
//For loop 
for (let i=0; i<= adultAge.length-1; i++){
	if(adultAge[i] < 20){
		continue;
	}
	console.log(adultAge[i]);
};



/*
Instructions:

2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
	the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/
let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
	//add solutions here
  //you can refer on our last discussion yesterday
  for (let i=0; i<= students.length-1; i++){
  	if(students[i] == studentName){
  		console.log(students[i]);
  		break;
  	}
  };
}

searchStudent('Jazz'); //invoked function with a given argument 'Jazz'

/*
	Sample output:
  
  Jazz
*/